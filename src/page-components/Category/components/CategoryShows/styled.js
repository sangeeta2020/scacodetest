import styled from 'styled-components';
import { Box } from '@rebass/grid';
import spacing from 'styling/spacing';
import screen from 'styling/screen';
import Image from 'shared-components/Image';

export const StyledCategoryShows = styled(Box)`
  background-color: ${(props) => props.theme.dark};
  border-radius: 26px;
  width: fit-content;
  padding: ${spacing.m};

  ${screen.tablet} {
    padding: 15px;
  }
`;

export const TextWrapper = styled.div`
  margin-bottom: 12px;

  ${screen.mobile} {
    margin-bottom: 0;
  }
`;

export const Card = styled.div`
  display: grid;
  max-width: 100%;

  grid-template-columns: 1fr;
  gap: 10px;
  margin-left: 10px;
  margin-right: 10px;

  //default pixel size: 16px
  // mobile: 640px
  ${screen.mobile} {
    grid-template-columns: repeat(2, 1fr);
  }

  //tablet: 832px
  ${screen.tablet} {
    grid-template-columns: repeat(3, 1fr);
  }

  //laptop: 1024px
  ${screen.laptop} {
    grid-template-columns: repeat(4, 1fr);
  }

  //desktop: 1216px
  ${screen.desktop} {
    grid-template-columns: repeat(5, 1fr);
  }
`;

export const CardWrapper = styled.div`
  overflow: hidden;
  box-shadow: 0px 3px 12px rgba(0, 0, 0, 0.15);
  transition: all 150ms ease-in-out;

  &:hover {
    transform: scale(1.05);
  }
`;

export const ImageWrapper = styled(Image)`
  border-radius: 8px;
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center center;
  height: auto;
`;
