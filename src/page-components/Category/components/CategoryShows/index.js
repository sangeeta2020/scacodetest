import React, { useEffect, useState } from 'react';
import PropTypes, { bool, func } from 'prop-types';
import { Flex } from '@rebass/grid';
import Paragraph from 'shared-components/Typography/Paragraph';
import SortButton from 'shared-components/SortButton';

import {
  StyledCategoryShows,
  TextWrapper,
  Card,
  CardWrapper,
  ImageWrapper,
} from './styled';

function CategoryShows({ shows, description }) {
  const [sortedShows, setSortedShows] = useState([...shows]);

  const sortOptions = [
    {
      key: 'asc',
      value: 'Sort A-Z',
    },
    {
      key: 'desc',
      value: 'Sort Z-A',
    },
  ];

  function getSortedShowsByName(sortType) {
    const sortedItems = [...shows];
    const isAsc = sortType === 'asc' ? 1 : -1;
    sortedItems.sort((a, b) => (a.name < b.name ? -1 : 1) * isAsc);

    setSortedShows(sortedItems);
    return sortedItems;
  }

  useEffect(() => {
    setSortedShows(sortedShows);
  }, []);

  return (
    <StyledCategoryShows>
      <Flex justifyContent="space-between" alignItems="center" flexWrap="wrap">
        {sortedShows && (
          <>
            <Paragraph
              text={`${sortedShows.length} Podcasts`}
              variant="l"
              mb="l"
              mt="l"
            />
            <TextWrapper>
              <SortButton
                onOptionClick={getSortedShowsByName}
                options={sortOptions}
                style={{ textAlign: 'right' }}
              />
            </TextWrapper>

            <Card>
              {sortedShows.map((show) => (
                <CardWrapper key={show.id}>
                  <ImageWrapper src={show.images.squareLarge.url} />
                  <TextWrapper>
                    <Paragraph
                      text={show.name}
                      linesToShow={1}
                      variant="l"
                      mt="m"
                      mb="m"
                    />
                    <Paragraph
                      text={show.description}
                      linesToShow={3}
                      variant="s"
                      mb="l"
                      transparent
                    />
                  </TextWrapper>
                </CardWrapper>
              ))}
            </Card>
          </>
        )}
      </Flex>
    </StyledCategoryShows>
  );
}

CategoryShows.propTypes = {
  shows: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string,
      name: PropTypes.string,
      description: PropTypes.string,
      images: PropTypes.shape({
        squareLarge: PropTypes.shape({
          url: PropTypes.string,
        }),
      }),
    })
  ),
  description: PropTypes.string,
};

CategoryShows.defaultProps = {
  shows: [],
  description: null,
};

export default CategoryShows;
